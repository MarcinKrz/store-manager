drop index id_boss_idx;

drop table shops CASCADE;
drop table jobs CASCADE;
drop table reports CASCADE;
drop table employees CASCADE;
drop table contracts CASCADE;




create table jobs(
job_title varchar(50) NOT NULL,
salary_min numeric(7,2) NOT NULL,
salary_max numeric(7,2) NOT NULL,
PRIMARY KEY(job_title) );

CREATE TABLE shops (
	nip varchar(10) NOT NULL,
	name varchar(50) NOT NULL,
	address varchar(50) NOT NULL,
	CONSTRAINT check132 CHECK ((length((nip)::text) = 10)),
	CONSTRAINT shops_nip_check CHECK (((nip)::text ~ '^[0-9\.]+$'::text)),
	CONSTRAINT shops_pkey PRIMARY KEY (nip)
);


create table employees(
id_employee serial NOT NULL,
name varchar(50) NOT NULL,
surname varchar(50) NOT NULL,
address varchar(50) NOT NULL,
id_boss integer NULL,
password varchar(50) NOT NULL,
primary key(id_employee),
foreign key(id_boss) references employees(id_employee)
);


create table reports(
id_shop varchar(10) NOT NULL,
id_employee integer NOT NULL,
date date NOT NULL,
income numeric(7,2) NOT NULL,
costs numeric(7,2) NOT NULL,
sold integer NOT NULL,
primary key(date,id_employee,id_shop),
foreign key (id_employee) references employees(id_employee) ON DELETE CASCADE,
foreign key (id_shop) references shops(nip) ON DELETE CASCADE
);

create table contracts(
id_shop varchar(10) NOT NULL,
id_employee integer NOT NULL,
id_job varchar(50) NOT NULL,
from_date date NOT NULL,
to_date date NOT NULL,
salary numeric(7,2) NOT NULL,
primary key (from_date,id_employee,id_shop,id_job),
foreign key (id_employee) references employees(id_employee) ON DELETE CASCADE,
foreign key (id_shop) references shops(nip) ON DELETE CASCADE,
foreign key (id_job) references jobs(job_title) ON DELETE CASCADE
);

--indexes
create index id_boss_idx  on employees(id_boss);






