CREATE OR REPLACE FUNCTION public.profit_employee(id_emp integer, from_date date, to_date date)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
declare
	profit numeric(7,2);
begin
	select sum(income-costs) into profit from reports r where r.id_employee = id_emp and r.date>=from_date and r.date<=to_date;

	return profit;
end;
$function$
;

