CREATE OR REPLACE FUNCTION public.raise_thebest_employess(idshop character varying, job_name character varying, raise numeric, threshold numeric, fromdate date, todate date)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
	declare
	id_e integer;
	cursor_emp cursor for select id_employee from employees join contracts using(id_employee)
	where id_shop = idshop
	and id_job=job_name
	and from_date <=current_date
	and to_date >=current_date;					
begin
	open cursor_emp;
	loop
	fetch cursor_emp into id_e;
	exit when not found;
	
	if profit_employee(id_e,fromdate,todate)>threshold then
		UPDATE contracts SET salary = salary+raise
		WHERE id_employee=id_e and id_job=job_name
		and from_date<=current_date
		and to_date>=current_date;
	end if;
	
	end loop;


	close cursor_emp;
	
end;
$function$
;

