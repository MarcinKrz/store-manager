CREATE OR REPLACE FUNCTION public.avg_profit_shop(idshop character varying)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
declare
	profit numeric(7,2);
begin
	select coalesce(avg(income-costs),0) into profit from reports r 
	where r.nip = idshop;

	return profit;
end;
$function$
;



CREATE OR REPLACE FUNCTION public.delete_reports(idshop character varying)
 RETURNS void
 LANGUAGE plpgsql
AS $function$			
begin	
	delete from reports where nip=idShop;
end;
$function$
;

