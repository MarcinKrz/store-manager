CREATE TABLE public.employees (
	id_employee serial NOT NULL,
	"name" varchar(50) NOT NULL,
	surname varchar(50) NOT NULL,
	address varchar(50) NOT NULL,
	"password" varchar(50) NOT NULL,
	CONSTRAINT employees_pkey PRIMARY KEY (id_employee)
);


CREATE TABLE public.jobs (
	job_title varchar(50) NOT NULL,
	salary_min numeric(7,2) NOT NULL,
	salary_max numeric(7,2) NOT NULL,
	CONSTRAINT jobs_pkey PRIMARY KEY (job_title)
);


CREATE TABLE public.contracts (
	nip varchar(10) NOT NULL,
	id_employee int4 NOT NULL,
	job_title varchar(50) NOT NULL,
	from_date timestamp NOT NULL,
	to_date timestamp NOT NULL,
	salary numeric(7,2) NOT NULL,
	CONSTRAINT contracts_pkey PRIMARY KEY (from_date, id_employee, nip, job_title),
	CONSTRAINT contracts_id_employee_fkey FOREIGN KEY (id_employee) REFERENCES employees(id_employee),
	CONSTRAINT contracts_id_job_fkey FOREIGN KEY (job_title) REFERENCES jobs(job_title),
	CONSTRAINT contracts_id_shop_fkey FOREIGN KEY (nip) REFERENCES shops(nip)
);
CREATE INDEX contracts_id_emp_date ON public.contracts USING btree (id_employee, from_date, to_date);
CREATE INDEX contracts_id_shop_date ON public.contracts USING btree (nip, from_date, to_date);


CREATE TABLE public.shops (
	nip varchar(10) NOT NULL,
	"name" varchar(50) NOT NULL,
	address varchar(50) NOT NULL,
	"password" varchar(50) NOT NULL,
	CONSTRAINT check132 CHECK ((length((nip)::text) = 10)),
	CONSTRAINT shops_nip_check CHECK (((nip)::text ~ '^[0-9\.]+$'::text)),
	CONSTRAINT shops_pkey PRIMARY KEY (nip)
);




CREATE TABLE public.reports (
	nip varchar(10) NOT NULL,
	id_employee int4 NOT NULL,
	"date" timestamp NOT NULL,
	income numeric(7,2) NOT NULL,
	costs numeric(7,2) NOT NULL,
	sold int4 NOT NULL,
	CONSTRAINT reports_pkey PRIMARY KEY (nip, date, id_employee),
	CONSTRAINT reports_id_employee_fkey FOREIGN KEY (id_employee) REFERENCES employees(id_employee),
	CONSTRAINT reports_id_shop_fkey FOREIGN KEY (nip) REFERENCES shops(nip)
);




