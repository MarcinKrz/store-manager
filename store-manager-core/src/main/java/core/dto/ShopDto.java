package core.dto;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;

@Component
public class ShopDto {

    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    public Integer profit(String nip) {
        Query a = entityManager.createNativeQuery("select avg_profit_shop( ?1 )")
                .setParameter(1, nip);
        return ((BigDecimal) a.getResultList().get(0)).intValue();

    }

    @Transactional
    public void deleteReport(String nip) {
//        Query a = entityManager.createNativeQuery("select delete_reports( ?1 )")
//                .setParameter(1, nip);
//        a.getResultStream();

        entityManager.createNativeQuery("select CAST(delete_reports( ?1 ) as varchar)")
                .setParameter(1, nip).getResultList();

//        entityManager.createNativeQuery("select count(*) from delete_reports( ?1 )")
//                .setParameter(1, nip).getResultList();
//
    }

}
