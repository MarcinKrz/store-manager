package core.dto;

import core.domain.contract.Contract;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ContractDto {


    private String name;
    private String surname;
    private String job_title;
    private Date from_date;
    private Date to_date;
    private Float salary;

}
