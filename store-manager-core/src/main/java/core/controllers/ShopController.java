package core.controllers;

import core.domain.shop.Shop;
import core.domain.shop.ShopRepository;
import core.dto.ShopDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping("/shops")
@CrossOrigin("http://localhost:4200")
@AllArgsConstructor
@Slf4j
public class ShopController {

    private final ShopRepository shopRepository;
    private final ShopDto shopDto;

    @GetMapping(value = "all", produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Page<Shop>> getAllShops(Pageable pageable) {
        Page<Shop> shops = shopRepository.findAllByOrderByNip(pageable);
        return ResponseEntity.ok(shops);
    }


    @GetMapping(value = "filtered", produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Page<Shop>> getShops(@RequestParam("query") String query, Pageable pageable) {
        Page<Shop> shops = shopRepository.findBla(query, pageable);
        return ResponseEntity.ok(shops);
    }


    @DeleteMapping(value = "/delete/{id}", produces = "application/json")
    public ResponseEntity deleteShop(@PathVariable("id") String id) {
        try{
            shopRepository.deleteById(id);
            return ResponseEntity.ok().build();
        }catch (Exception e){
            return ResponseEntity.status(422).build();
        }


    }


    @PostMapping
    public ResponseEntity createShop(@RequestBody Shop shop) {
        log.info("tworzenie nowego sklepu");
        if (shopRepository.existsById(shop.getNip())) {
            return ResponseEntity.status(422).build();
        } else {
            shopRepository.save(shop);
            return ResponseEntity.ok().build();
        }
    }

    @PutMapping
    public ResponseEntity updateShop(@RequestBody Shop shop) {
            shopRepository.save(shop);
            return ResponseEntity.ok().build();
        }

        @GetMapping(value="/profit")
    public ResponseEntity<Integer> executeFunction(@RequestParam("nip") String nip){
        return ResponseEntity.ok(shopDto.profit(nip));
        }

        @GetMapping(value="/deleteReports")
        public ResponseEntity deleteReports(@RequestParam("nip") String nip){
            shopDto.deleteReport(nip);
            return ResponseEntity.ok().build();
        }



//    @PutMapping
//    public ResponseEntity updateShop(@RequestBody Shop shop){
//        shopRepository
//    }


    //    @GetMapping("/reports")
//    public ResponseEntity<List<Report>> getReports(@RequestParam(value = "nip") String nip) {
//        return shopRepository.findById(nip)
//                .map(shop -> ResponseEntity.ok(shop.getReports()))
//                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
//
//    }


//    @GetMapping("/allshops")
//    public ResponseEntity<List<Shop>> getShops(){
//        return ResponseEntity.ok(shopRepository.findAll());
//    }

}
