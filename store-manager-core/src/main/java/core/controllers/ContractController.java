package core.controllers;

import core.domain.contract.Contract;
import core.domain.contract.ContractPk;
import core.domain.contract.ContractRespository;
import core.domain.contract.ContractWithNamesDto;
import core.domain.job.Job;
import core.domain.job.JobRepository;
import core.domain.shop.Shop;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;


@RestController
@RequestMapping("/contracts")
@CrossOrigin("http://localhost:4200")
@AllArgsConstructor
@Slf4j
public class ContractController {

    private ContractRespository contractRespository;

    //public ResponseEntity<Page<Employee>>

//    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
//    public ResponseEntity<List<ContractDto>> getContracts(){
//        List<Contract> contract = contractRespository.findAll();
//
//        return ResponseEntity.ok(contractDtos);
    //}

    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Page<ContractWithNamesDto>> getAllFiltered(@RequestParam("query") String query, Pageable pageable) {
        Page<ContractWithNamesDto> blaBla = contractRespository.findContractsToTable(query, pageable);
        return ResponseEntity.ok(blaBla);
    }

    @GetMapping(value="shopsEmployee",produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Shop>> getShopsWhereWork(@RequestParam("query") Long idEmployee){
        return ResponseEntity.ok(contractRespository.findShopWhereWork(idEmployee));
    }



    @DeleteMapping
    public ResponseEntity delete(@RequestParam("nip") String nip, @RequestParam("jobTitle") String jobTitle,
                                 @RequestParam("fromDate") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS") LocalDateTime fromDate,
                                 @RequestParam("idEmployee") Long idEmployee) {
        ContractPk contractPk = new ContractPk(nip, idEmployee, jobTitle, fromDate); //asdasdsa
        contractRespository.deleteById(contractPk);
        return ResponseEntity.ok().build();
    }


//    @PostMapping
//    public ResponseEntity create(@RequestBody Contract contract) {
//        if (contractRespository.existsById(contract.getId())) {
//            return ResponseEntity.status(422).build();
//        } else {
//            log.info("przed");
//            Optional<Job> job = jobRepository.findById(contract.getId().getJobTitle())
//                    .filter(c -> contract.getSalary() >= c.getSalaryMin())
//                    .filter(c -> contract.getSalary() <= c.getSalaryMax());
//            log.info("asd");
//            if(job.isPresent()){
//                contractRespository.save(contract);
//                return  ResponseEntity.ok().build();
//            }
//            else{
//                return ResponseEntity.status(423).build();
//            }
//
//        }
//    }


    @PostMapping
    public ResponseEntity create(@RequestBody Contract contract) {
        contract.getId().setFromDate(contract.getId().getFromDate().truncatedTo(ChronoUnit.DAYS)
                .plus(1, ChronoUnit.DAYS));
        contract.setToDate(contract.getToDate().truncatedTo(ChronoUnit.DAYS)
                .plus(1, ChronoUnit.DAYS));

        if(contractRespository.existsById(contract.getId())){
            return ResponseEntity.status(422).build();
        }
        contractRespository.save(contract);
        return ResponseEntity.ok().build();

    }
    @PutMapping
    public ResponseEntity update(@RequestBody Contract contract) {
        contract.setToDate(contract.getToDate().truncatedTo(ChronoUnit.DAYS)
                .plus(1, ChronoUnit.DAYS));

        contractRespository.save(contract);
        return ResponseEntity.ok().build();

    }
}
