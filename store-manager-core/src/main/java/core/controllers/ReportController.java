package core.controllers;

import core.domain.contract.Contract;
import core.domain.contract.ContractPk;
import core.domain.contract.ContractRespository;
import core.domain.employee.EmployeeRepository;
import core.domain.job.JobRepository;
import core.domain.report.Report;
import core.domain.report.ReportPk;
import core.domain.report.ReportRepository;
import core.domain.report.ReportWithNamesDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping("reports")
@CrossOrigin("http://localhost:4200")
@AllArgsConstructor
@Slf4j
public class ReportController {

    private ReportRepository reportRepository;
    private EmployeeRepository employeeRepository;
    private ContractRespository contractRespository;

    @GetMapping
    public ResponseEntity<Page<ReportWithNamesDto>> getAllFiltered(@RequestParam("query") String query, Pageable pageable) {
        return ResponseEntity.ok(reportRepository.findAllWithNamesFilterd(query, pageable));
    }

//
//    @GetMapping(value = "/{idShop}&{idEmp}", produces = APPLICATION_JSON_UTF8_VALUE)
//    public ResponseEntity<Page<Report>> getReports(@PathVariable(value = "idShop") String idShop,
//                                                   @PathVariable(value = "idEmp") Long idEmployee,
//                                                   Pageable pageable) {
//        Page<Report> byId_idShopAndAndEmployee = reportRepository.findById_NipAndId_IdEmployee(idShop, idEmployee, pageable);
//        return ResponseEntity.ok(byId_idShopAndAndEmployee);
//    }


    @PostMapping
    public ResponseEntity postReports(@RequestBody Report report) {
        report.getId().setDateReport(report.getId().getDateReport()
                .truncatedTo(ChronoUnit.DAYS)
                .plus(1, ChronoUnit.DAYS));
        if (reportRepository.existsById(report.getId())) {
            return ResponseEntity.status(422).build();
        } else {
            List<Contract> contractList = contractRespository.findByEmployeeAndShopForDay(report.getId().getIdEmployee(), report.getId().getNip(), report.getId().getDateReport());
//            long count = contractList.stream()
//                    .filter(c -> report.getId().getDateReport().compareTo(c.getId().getFromDate()) >= 0)
//                    .filter(c -> report.getId().getDateReport().compareTo(c.getId().getFromDate()) <= 0)
//                    .count();
            if (!contractList.isEmpty()) {
                reportRepository.save(report);
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.status(423).build();
            }

        }
    }

    @PutMapping
    public ResponseEntity updateReports(@RequestBody Report report) {
        reportRepository.save(report);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping
    public ResponseEntity delete(@RequestParam("nip") String nip, @RequestParam("idEmployee") Long idEmployee,
                                 @RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime date) {
        ReportPk reportPk = new ReportPk(nip,date,idEmployee);
        reportRepository.deleteById(reportPk);
        return ResponseEntity.ok().build();
    }


}
