package core.controllers;


import core.domain.employee.Employee;
import core.domain.employee.EmployeeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping("/employees")
@CrossOrigin("http://localhost:4200")
@AllArgsConstructor
@Slf4j
public class EmployeeController {

    private EmployeeRepository employeeRepository;


    //pobranie wszystkich pracownikow
    @GetMapping(value="/filtered",produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Page<Employee>> getAllFilter(@RequestParam("query") String query, Pageable pageable) {
        Page<Employee> page = employeeRepository.findAllFiltered(query,pageable);
        return ResponseEntity.ok(page);

    }

    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<Employee>> getAll() {
        return ResponseEntity.ok(employeeRepository.findAllSort());
    }


    //usuniecie pracownika o podanym id
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity deleteEmployee(@PathVariable(value = "id") Long employeeId) {

        try {
            employeeRepository.deleteById(employeeId);
            return ResponseEntity.ok().build();
        }
        catch(Exception e){
        return ResponseEntity.status(422).build();
    }
    }


    @PostMapping
    public ResponseEntity addEmployee(@RequestBody Employee employee) {
        if (employeeRepository.existsById(employee.getId())) {
            return ResponseEntity.status(422).build();
        } else {
            employeeRepository.save(employee);
            return ResponseEntity.ok().build();
        }
    }

    @PutMapping
    public ResponseEntity updateEmployee(@RequestBody Employee employee) {
        employeeRepository.save(employee);
        return ResponseEntity.ok().build();
    }


    //nie uzywam na razie
    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Employee> getEmployee(@PathVariable(value = "id") long employeeId) {
        return employeeRepository.findById(employeeId)
                .map(employee -> ResponseEntity.ok(employee))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


      /*
    @GetMapping(value = "/{id}/subordinates", produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Page<Employee>> getSubordinates(@PathVariable(value = "id") Long employeeId, Pageable pageable) {
        Page<Employee> subordinates = employeeRepository.findByBossId(employeeId, pageable);
        return ResponseEntity.ok(subordinates);
    }
 */
}
