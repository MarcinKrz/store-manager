package core.controllers;


import core.domain.job.Job;
import core.domain.job.JobRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping("jobs")
@CrossOrigin("http://localhost:4200")
@AllArgsConstructor
@Slf4j
public class JobController {

    private JobRepository jobRepository;

//    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
//    public ResponseEntity<Page<Job>> getAll(Pageable pageable) {
//        Page<Job> page = jobRepository.findAllByOrderByJobTitle(pageable);
//        return ResponseEntity.ok(page);
//    }


    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Page<Job>> getAll(@RequestParam("query") String query, Pageable pageable) {
        Page<Job> page = jobRepository.findAllFiltered(query, pageable);
        return ResponseEntity.ok(page);
    }


    @PostMapping
    public ResponseEntity save(@RequestBody Job job) {
        if (jobRepository.existsById(job.getJobTitle())) {
            return ResponseEntity.status(422).build();
        } else {
            jobRepository.save(job);
            return ResponseEntity.ok().build();
        }
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Job job) {
        jobRepository.save(job);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/{jobTitle}")
    public ResponseEntity delete(@PathVariable("jobTitle") String jobTitle) {
        try {
            jobRepository.deleteById(jobTitle);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.status(422).build();
        }
    }

    @GetMapping(value = "/salary")
    public ResponseEntity getSalary(@RequestParam("jobTitle") String jobTitle) {
        return ResponseEntity.ok(jobRepository.findById(jobTitle));
    }


}
