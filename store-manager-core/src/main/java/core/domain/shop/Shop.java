package core.domain.shop;

import com.fasterxml.jackson.annotation.JsonIgnore;
import core.domain.contract.Contract;
import core.domain.report.Report;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "shops")
@Builder
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Shop {

    @Id
    @Column(name = "nip")
    private String nip;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name="password")
    private String password;

    @JsonIgnore
    @OneToMany(mappedBy = "shop")
    private List<Contract> contracts;

    @JsonIgnore
    @OneToMany(mappedBy = "shop")
    private List<Report> reports;

}
