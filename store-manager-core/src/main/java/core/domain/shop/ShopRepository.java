package core.domain.shop;

import org.hibernate.annotations.NamedNativeQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.lang.annotation.Native;

public interface ShopRepository extends JpaRepository<Shop, String> {
    Page<Shop> findAllByOrderByNip(Pageable pageable);
    Page<Shop> findByNameContainsOrderByNip(String name,Pageable pageable);

    @Query("select s from Shop s where s.name like %:queryString% or s.address like %:queryString% or s.nip like %:queryString% order by s.name, s.address,s.password ")
    Page<Shop> findBla(@Param("queryString") String queryString, Pageable pageable);


//    @NamedNativeQuery("select avg_profit_shop(:query)")
//    Float executeFunction(@Param("query") String query);
}
