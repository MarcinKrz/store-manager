package core.domain.contract;

import core.domain.shop.Shop;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface ContractRespository extends JpaRepository<Contract, ContractPk> {

    @Query("select new core.domain.contract.ContractWithNamesDto(c.id, c.toDate, c.salary, c.shop.name,c.employee.surname, c.employee.name) from Contract c" +
            " where c.shop.name like %:queryString%  or c.employee.surname like %:queryString% or c.id.jobTitle like %:queryString% order by c.shop.name, c.employee.surname")
    Page<ContractWithNamesDto> findContractsToTable(@Param("queryString") String queryString,Pageable pageable);

    @Query("select distinct c.shop from Contract c where c.id.idEmployee = :query")
    List<Shop> findShopWhereWork(@Param("query") Long query);

    @Query(" select c from Contract c  where c.employee.id = :employeeId and c.shop.nip = :shopNip and " +
            "c.id.fromDate <= :day and c.toDate >= :day")
    List<Contract> findByEmployeeAndShopForDay(@Param("employeeId") Long employeeId, @Param("shopNip") String shopNip,
                                               @Param("day")LocalDateTime day);


}

