package core.domain.contract;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;


@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ContractPk implements Serializable {

    @Column(name = "nip")
    private String nip;

    @Column(name = "id_employee")
    private Long idEmployee;

    @Column(name = "job_title")
    private String jobTitle;

    @Column(name = "from_date")
    @DateTimeFormat(pattern = "yyyy-MM-ddTHH:mm:ssZ")
    private LocalDateTime fromDate;
}
