package core.domain.contract;

import core.domain.employee.Employee;
import core.domain.job.Job;
import core.domain.shop.Shop;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "contracts")
@NoArgsConstructor
@Getter
@Setter
public class Contract {

    @EmbeddedId
    ContractPk id;

    @Column(name = "to_date")
    @DateTimeFormat(pattern = "yyyy-MM-ddTHH:mm:ssZ")
    private LocalDateTime toDate;

    @Column(name = "salary")
    private Float salary;

    @ManyToOne
    @JoinColumn(nullable = false, name = "nip", insertable = false, updatable = false)
    private Shop shop;


    @ManyToOne
    @JoinColumn(nullable = false, name = "id_employee", insertable = false, updatable = false)
    private Employee employee;

    @ManyToOne
    @JoinColumn(nullable = false, name = "job_title", insertable = false, updatable = false)
    private Job job;


    @Override
    public String toString() {
        return "Contract{" +
                "id=" + id +
                ", salary=" + salary +
                ". shop = " + shop.getName() +
               // ". empl = " + employee.getId() + " pesel: " + employee.getIdPerson() +
                '}';
    }
}
