package core.domain.contract;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContractWithNamesDto {
    private ContractPk id;
    @DateTimeFormat(pattern = "yyyy-MM-ddTHH:mm:ssZ")
    private LocalDateTime toDate;
    private Float salary;
    private String shopName;
    private String surnameEmployee;
    private String nameEmployee;
}
