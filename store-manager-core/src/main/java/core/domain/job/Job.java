package core.domain.job;

import com.fasterxml.jackson.annotation.JsonIgnore;
import core.domain.contract.Contract;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Jobs")
@NoArgsConstructor
@Setter
@Getter
public class Job {

    @Id
    @Column(name = "job_title")
    private String jobTitle;

    @Column(name = "salary_min")
    private Float salaryMin;

    @Column(name = "salary_max")
    private Float salaryMax;

    @JsonIgnore
    @OneToMany(mappedBy = "job")
    private List<Contract> contracts;
}
