package core.domain.job;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestParam;

public interface JobRepository extends JpaRepository<Job,String> {

    @Query("select j from Job j where j.jobTitle like %:filter%  order by j.jobTitle ")
    Page<Job> findAllFiltered(@Param("filter") String filter, Pageable pageable);


}
