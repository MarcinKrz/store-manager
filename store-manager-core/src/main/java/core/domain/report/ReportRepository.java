package core.domain.report;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ReportRepository extends JpaRepository<Report, ReportPk> {
    Page<Report> findById_NipAndId_IdEmployee(String idShop, Long idEmployee, Pageable pageable);

    @Query("select new core.domain.report.ReportWithNamesDto(r.id,r.income,r.costs,r.sold,r.shop.name,r.employee.surname,r.employee.name) from Report r" +
            " where r.shop.name like %:filter% or r.employee.name like %:filter% or r.employee.surname like %:filter%")
    public Page<ReportWithNamesDto> findAllWithNamesFilterd(@Param("filter") String filter, Pageable pageable);
}
