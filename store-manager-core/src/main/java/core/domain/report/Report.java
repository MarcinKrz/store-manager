package core.domain.report;

import com.fasterxml.jackson.annotation.JsonIgnore;
import core.domain.employee.Employee;
import core.domain.shop.Shop;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


@Entity
@Table(name = "reports")
@NoArgsConstructor
@Getter
@Setter
public class Report {

    @EmbeddedId
    private ReportPk id;

    @Column(name = "income")
    private Float income;

    @Column(name = "costs")
    private Float costs;

    @Column(name = "sold")
    private Integer sold;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(nullable = false, name = "nip", referencedColumnName = "nip", insertable = false, updatable = false)
    private Shop shop;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(nullable = false,name = "id_employee", referencedColumnName = "id_employee",insertable = false, updatable = false)
    private Employee employee;


}
