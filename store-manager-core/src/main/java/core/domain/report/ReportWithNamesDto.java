package core.domain.report;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportWithNamesDto {
    private ReportPk id;
    private Float income;
    private Float costs;
    private Integer sold;
    private String shopName;
    private String surnameEmployee;
    private String nameEmployee;
}
