package core.domain.report;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ReportPk implements Serializable {

    @Column(name = "nip")
    private String nip;

    @Column(name = "date")
    private LocalDateTime dateReport;

    @Column(name = "id_employee")
    private Long idEmployee;
}
