package core.domain.employee;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import core.domain.contract.Contract;
import core.domain.report.Report;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "employees")
@NoArgsConstructor
@Setter
@Getter
@ToString
public class Employee {


    @Id
    @SequenceGenerator(name = "employees_id_employee_seq", sequenceName = "employees_id_employee_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employees_id_employee_seq")
    @Column(name = "id_employee")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "address")
    private String address;

    @Column(name = "password")
    private String password;

    @JsonIgnore
    @OneToMany(mappedBy = "employee")
    private List<Contract> contracts;

    @JsonIgnore
    @OneToMany(mappedBy = "employee")
    private List<Report> reports;


}
