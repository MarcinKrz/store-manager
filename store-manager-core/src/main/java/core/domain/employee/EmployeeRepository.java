package core.domain.employee;

import core.domain.contract.Contract;
import core.domain.shop.Shop;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    // Page<Employee> findByBossId(Long bossId, Pageable pageable);
    Page<Employee> findAllByOrderById(Pageable pageable);


    @Query("select e from Employee e where e.name like %:filter% or e.surname like %:filter% or e.address like %:filter% or e.password like %:filter% order by e.surname, e.name, e.address,e.password")
    Page<Employee> findAllFiltered(@Param("filter") String filter,Pageable pageable);

    @Query("select e from Employee e order by e.surname,e.name")
    List<Employee>findAllSort();


    @Query(" select e.contracts from Employee  e  where e.id = :employeeId ")
    List<Contract> findContracts(@Param("employeeId") Long employeeId);
}
