drop function profit_employee;

create or replace function profit_employee(id_emp integer,from_date date,to_date date)
	returns numeric(7,2) as $profit$
declare
	profit numeric(7,2);
	sum_income numeric(7,2);
	sum_costs numeric(7,2);
begin
	select sum(income) into sum_income from reports r where r.id_employee = id_emp and r.date>=from_date and r.date<=to_date;
	select sum(costs) into sum_costs from reports r where r.id_employee = id_emp and r.date>=from_date and r.date<=to_date;
	profit := sum_income-sum_costs;

	return profit;
end;
$profit$ LANGUAGE plpgsql;
