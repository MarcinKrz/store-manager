import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSnackBar, MatSort} from '@angular/material';
import {JobsTableDataSource} from './jobs-table-datasource';
import {JobService} from "../services/job/job.service";
import {Job} from "../model/job";
import {EmployeeDialogComponent} from "../employees-table/employee-dialog/employee-dialog.component";
import {JobDialogComponent} from "./job-dialog/job-dialog.component";

@Component({
  selector: 'app-jobs-table',
  templateUrl: './jobs-table.component.html',
  styleUrls: ['./jobs-table.component.css'],
})
export class JobsTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: JobsTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['job_title', 'salary_min', 'salary_max','actions'];


  constructor(private jobService: JobService, private dialog: MatDialog,private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.dataSource = new JobsTableDataSource(this.paginator, this.sort, this.jobService);
  }

  delete(job: Job){
    this.jobService.delete(job).subscribe(success => {
      this.dataSource.reload();
      console.log('Usunieto');
    }, error => {
      this.snackBar.open("Nie możesz usunąć wybranego zawodu, najpierw usuń wszystkich pracowników zatrudnionych na tym stanowisku");
      console.log('Blad usuwania');
    });
  }

  openDialog(selectJob): void{
    let dialogRef = this.dialog.open(JobDialogComponent, {
      height: '460px',
      width: '400px',
      data: {job: selectJob}
    });
    dialogRef.afterClosed()
      .subscribe( value => this.dataSource.reload());
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.reload();
  }
}
