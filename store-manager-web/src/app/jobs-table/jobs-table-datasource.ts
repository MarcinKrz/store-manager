import {DataSource} from '@angular/cdk/collections';
import {MatPaginator, MatSort} from '@angular/material';
import {map, startWith, switchMap} from 'rxjs/operators';
import {merge, Observable, Subject} from 'rxjs';
import {Job} from "../model/job";
import {JobService} from "../services/job/job.service";


/**
 * Data source for the JobsTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class JobsTableDataSource extends DataSource<Job> {

  private trigger = new Subject();
  public totalElements: number = 0;
  public filter: string = '';


  constructor(private paginator: MatPaginator, private sort: MatSort, private jobService: JobService) {
    super();
  }

  connect(): Observable<Job[]> {
    return merge(this.trigger, this.sort.sortChange, this.paginator.page).pipe(
      startWith(null),
      switchMap(() => this.jobService.getAll(this.filter,this.paginator.pageIndex, this.paginator.pageSize)),
      map(page => {
        this.totalElements = page.totalElements;
        return page.content;
      })
    );
  }


  disconnect() {
  }

  public reload() {
    this.trigger.next();
  }

}
