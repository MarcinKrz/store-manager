import {Component, Inject, OnInit} from '@angular/core';
import {JobService} from "../../services/job/job.service";
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from "@angular/material";
import {Job} from "../../model/job";


export interface DialogDataJob {
  job: Job;
}

@Component({
  selector: 'app-job-dialog',
  templateUrl: './job-dialog.component.html',
  styleUrls: ['./job-dialog.component.css']
})
export class JobDialogComponent implements OnInit {

  createDialog: boolean = true;

  job: Job = {
    jobTitle: '',
    salaryMin: 0,
    salaryMax: 0
  }

  constructor(@Inject(MAT_DIALOG_DATA) public  dialogDataJob: DialogDataJob,
              public matDialogRef: MatDialogRef<JobDialogComponent>,
              private jobService: JobService,private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    let selectJob: Job = this.dialogDataJob.job;
    if (selectJob != null) {
      this.createDialog = false;
      this.job.jobTitle = selectJob.jobTitle;
      this.job.salaryMin = selectJob.salaryMin;
      this.job.salaryMax = selectJob.salaryMax;
    }
  }

  onNoClick() {
    this.matDialogRef.close();
  }

  validationInput() {
    if (
      this.job.jobTitle != '' && this.job.salaryMin >= 0 && this.job.salaryMax >= this.job.salaryMin) {
      return true;
    }
    else false;
  }

  create() {
    if (this.createDialog) {
      this.jobService.create(this.job).subscribe(succes => {
          console.log("poprawnie stworzono sklep");
          this.matDialogRef.close();
        }, error1 => {
        if (error1.status == 422) {
          this.snackBar.open("Zawód o podanej nazwie już istnieje!!")
          console.log("blad tworzenia sklepu");
        }
        else {
          this.snackBar.open("Nieudana próba stworzenia zawodu, sprawdź czy poprawnie uzupełniłeś wszystkie pola")
        }
        }
      )
    }
    else {
      this.jobService.update(this.job).subscribe(succes => {
          console.log("poprawnie stworzono sklep");
          this.matDialogRef.close();
        }, error1 => {
        this.snackBar.open("Błąd modyfikacji zawodu, sprawdź czy poprawnie uzupełniłeś wszystkie pola")
        }
      )
    }
  }


}
