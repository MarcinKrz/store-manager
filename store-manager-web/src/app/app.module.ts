import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {MatTableModule} from '@angular/material/table';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeesComponent } from './employees/employees.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EmployeesTableComponent } from './employees-table/employees-table.component';
import {
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatButtonModule,
  MatInputModule,
  MatListModule,
  MatPaginatorModule,
  MatSortModule,
  MatTabsModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSelectModule,
  MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS, MAT_DATE_LOCALE
} from '@angular/material';
import { ReportComponent } from './report/report.component';
import { ShopsTableComponent } from './shops-table/shops-table.component';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import { ShopAddDialogComponent } from './shops-table/shop-add-dialog/shop-add-dialog.component';
import { NgKnifeModule } from 'ng-knife';
import { EmployeeDialogComponent } from './employees-table/employee-dialog/employee-dialog.component';
import { JobsTableComponent } from './jobs-table/jobs-table.component';
import { ContractsTableComponent } from './contracts-table/contracts-table.component';
import { JobDialogComponent } from './jobs-table/job-dialog/job-dialog.component';
import { ContractDialogComponent } from './contracts-table/contract-dialog/contract-dialog.component';
import {ReportsTableComponent} from './reports-table/reports-table.component';
import {ReportDialogComponent} from './reports-table/report-dialog/report-dialog.component';
import { MatMomentDateModule, MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import {DatePipe} from "@angular/common";

@NgModule({
  declarations: [
    AppComponent,
    EmployeesComponent,
    EmployeesTableComponent,
    ReportComponent,
    ShopsTableComponent,
    ShopAddDialogComponent,
    EmployeeDialogComponent,
    JobsTableComponent,
    ContractsTableComponent,
    JobDialogComponent,
    ContractDialogComponent,
    ReportsTableComponent,
    ReportDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatTabsModule,
    MatListModule,
    MatIconModule,
    MatDialogModule,
    NgKnifeModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatSnackBarModule

],
  entryComponents:[ShopAddDialogComponent,EmployeeDialogComponent,JobDialogComponent,ContractDialogComponent,ReportDialogComponent],
  providers: [
    DatePipe,
    {
    provide: MAT_DIALOG_DEFAULT_OPTIONS,
    useValue: {hasBackdrop: false}
  },
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 5500}},
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
