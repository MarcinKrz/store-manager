import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Report} from "../../model/report";
import {ReportId} from "../../model/reportWithNames";


@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private httpClient: HttpClient) {
  }


  getAll(query: string, page: number = 0, size: number = 0): Observable<any> {
    return this.httpClient.get(`http://localhost:8080/reports`, {
      params: new HttpParams()
        .append('page', `${page}`)
        .append('size', `${size}`)
        .append('query', `${query}`)
    });
  }

  create(report: Report): Observable<any> {
    return this.httpClient.post("http://localhost:8080/reports", report);
  }

  update(report: Report): Observable<any> {
    return this.httpClient.put("http://localhost:8080/reports", report);
  }

  delete(reportId: ReportId): Observable<any> {
    return this.httpClient.delete("http://localhost:8080/reports", {
      params: new HttpParams()
        .append('nip', `${reportId.nip}`)
        .append('idEmployee', `${reportId.idEmployee}`)
        .append('date', `${reportId.dateReport}`)
    })
  }


}
