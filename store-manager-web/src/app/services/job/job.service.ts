import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Job} from "../../model/job";


@Injectable({
  providedIn: 'root'
})
export class JobService {

  constructor(private httpClient: HttpClient) { }


  getAll(query: string,page: number, size:number): Observable<any>{
    return this.httpClient.get('http://localhost:8080/jobs', { params: new HttpParams()
        .append('page',`${page}`)
        .append('size',`${size}`)
        .append('query',`${query}`)
    });
  }


  create(job: Job):Observable<any>{
    return this.httpClient.post(`http://localhost:8080/jobs`,job);
  }

  update(job: Job):Observable<any>{
    return this.httpClient.put(`http://localhost:8080/jobs`,job);
  }

  delete(job: Job):Observable<any>{
    return this.httpClient.delete(`http://localhost:8080/jobs/${job.jobTitle}`);
  }

  getJob(jobTitle: string):Observable<any>{
    return this.httpClient.get(`http://localhost:8080/jobs/salary`,{ params: new HttpParams()
        .append('jobTitle',`${jobTitle}`)

    })
  }

}
