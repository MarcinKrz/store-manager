import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {ContractDto, ContractPk} from "../../model/contractDto";
import {Contract} from "../../model/contract";
import {Page} from "../page";
import {DatePipe} from "@angular/common";

@Injectable({
  providedIn: 'root'
})
export class ContractService {

  constructor(private httpClient: HttpClient, private datePipe: DatePipe) { }

getAll(query: string, page: number =0, size: number = 0): Observable<Page<ContractDto>> {
  return this.httpClient.get<Page<ContractDto>>(`http://localhost:8080/contracts`, {
    params: new HttpParams()
      .append('page', `${page}`)
      .append('size', `${size}`)
      .append('query',`${query}`)
  })
}

getShopsWorker(query: number):Observable<any>{
    return this.httpClient.get(`http://localhost:8080/contracts/shopsEmployee`, {
      params: new HttpParams()
        .append('query',`${query}`)
    })
}


  delete(idContract: ContractPk):Observable<any>{
    console.log(idContract);
    //let time: string = {{idContract.fromDate | timezone }};
      return this.httpClient.delete(`http://localhost:8080/contracts`,{
        params: new HttpParams()
          .append('nip',`${idContract.nip}`)
          .append('idEmployee',`${idContract.idEmployee}`)
          .append('jobTitle',`${idContract.jobTitle}`)
          .append('fromDate',`${this.datePipe.transform(idContract.fromDate, 'yyyy-MM-ddTHH:mm:ss.SSS')}`)
      });
  }

  create(contract: Contract): Observable<any>{
    return this.httpClient.post(`http://localhost:8080/contracts`,contract);
  }

  update(contract: Contract): Observable<any>{
    return this.httpClient.put(`http://localhost:8080/contracts`,contract);
  }



}
