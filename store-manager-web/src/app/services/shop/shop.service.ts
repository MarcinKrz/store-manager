import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams} from "@angular/common/http";
import {Shop} from "../../model/shop";
import {Page} from "../page";
import {Observable} from "rxjs";
import {log} from "util";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ShopService {


  constructor(private httpClient: HttpClient) {
  }

  getAllShops(query:string,page: number = 0, size: number = 0): Observable<Page<Shop>> {
    return this.httpClient.get<Page<Shop>>('http://localhost:8080/shops/filtered', { params: new HttpParams()
        .append('page',`${page}`)
        .append('size', `${size}`)
        .append('query', `${query}`)
    });

  }


  delete(shop: Shop): Observable<any>{
    log(shop.nip);
    return this.httpClient.delete(`http://localhost:8080/shops/delete/${shop.nip}`);
  }

  createShop(shop: Shop): Observable<any>{
    return this.httpClient.post("http://localhost:8080/shops",shop);

  }

  updateShop(shop: Shop): Observable<any>{
    return this.httpClient.put("http://localhost:8080/shops",shop);

  }

  getTotalProfit(nip: string ):Observable<any>{
    return this.httpClient.get("http://localhost:8080/shops/profit",{
      params: new HttpParams()
        .append('nip',`${nip}`)
    })
  }

  deleteReports(nip: string ):Observable<any>{
    return this.httpClient.get("http://localhost:8080/shops/deleteReports",{
      params: new HttpParams()
        .append('nip',`${nip}`)
    })
  }
}
