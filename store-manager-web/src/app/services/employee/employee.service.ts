import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Page} from "../page";
import {Employee} from "../../model/employee";
import {Shop} from "../../model/shop";
import {log} from "util";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private httpClient: HttpClient) {
  }

  getAll(query: string, page: number = 0, size: number = 20): Observable<Page<Employee>> {
    //page=0&size=3&sort=firstName,DESC
      return this.httpClient
        .get<Page<Employee>>(`http://localhost:8080/employees/filtered`, { params: new HttpParams()
            .append('page', `${page}`)
            .append('size', `${size}`)
            .append('query',`${query}`)
        });
  }

  getEmployees():Observable<any>{
    return this.httpClient
      .get(`http://localhost:8080/employees`);
  }


  delete(employee : Employee):Observable<any>{
    return this.httpClient.delete(`http://localhost:8080/employees/${employee.id}`);
  }

  create(employee: Employee): Observable<any>{
    return this.httpClient.post('http://localhost:8080/employees',employee);
  }

  update(employee: Employee): Observable<any>{
    return this.httpClient.put('http://localhost:8080/employees',employee);
  }


}
