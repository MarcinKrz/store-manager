import {DataSource} from '@angular/cdk/collections';
import {MatPaginator, MatSort} from '@angular/material';
import {merge, Observable, Subject} from 'rxjs';
import {ReportWithNames} from "../model/reportWithNames";
import {ReportService} from "../services/report/report.service";
import {map, startWith, switchMap} from "rxjs/operators";


export class ReportsTableDataSource extends DataSource<ReportWithNames> {

  private trigger = new Subject();
  public totalElements: number = 0;
  public filter: string = '';

  constructor(private paginator: MatPaginator, private sort: MatSort, private reportService: ReportService) {
    super();
  }


  connect(): Observable<ReportWithNames[]> {
    return merge(this.trigger, this.paginator.page, this.sort.sortChange).pipe(
      startWith(null),
      switchMap(() => this.reportService.getAll(this.filter, this.paginator.pageIndex, this.paginator.pageSize)),
      map(page => {
        this.totalElements = page.totalElements;
        return page.content;
      })
    )
  }

  disconnect() {
  }

  public reload() {
    this.trigger.next();
  }

}

