import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {ReportsTableDataSource} from './reports-table-datasource';
import {ReportService} from "../services/report/report.service";
import {ReportWithNames} from "../model/reportWithNames";
import {ReportDialogComponent} from "./report-dialog/report-dialog.component";

@Component({
  selector: 'app-reports-table',
  templateUrl: './reports-table.component.html',
  styleUrls: ['./reports-table.component.css'],
})
export class ReportsTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: ReportsTableDataSource;

  displayedColumns = ['nameShop', 'employee', 'dateReport', 'income', 'costs', 'sold', 'actions'];

  // displayedColumns = ['nameShop', 'dateReport','nameEmployee', 'actions'];
//, 'surnameEmployee', 'nameEmployee', 'income', 'costs', 'sold'
  constructor(private reportService: ReportService, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.dataSource = new ReportsTableDataSource(this.paginator, this.sort, this.reportService);
  }

  delete(report: ReportWithNames) {
    console.log(report.id);
    this.reportService.delete(report.id).subscribe(success => {
      this.dataSource.reload();
      console.log('Usunieto');
    }, error => {
      console.log('Blad usuwania');
    });
  }

  openDialog(selectReport: ReportWithNames): void {
    let dialogRef = this.dialog.open(ReportDialogComponent, {
      height: '500px',
      width: '400px',
      data: {report: selectReport}
    });
    dialogRef.afterClosed()
      .subscribe(value => this.dataSource.reload());
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.reload();
  }


}
