import {Component, Inject, OnInit} from '@angular/core';
import {ReportId, ReportWithNames} from "../../model/reportWithNames";
import {FormControl} from "@angular/forms";
import {Shop} from "../../model/shop";
import {Employee} from "../../model/employee";
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from "@angular/material";
import {ContractService} from "../../services/contract/contract.service";
import {ShopService} from "../../services/shop/shop.service";
import {EmployeeService} from "../../services/employee/employee.service";
import {Report} from "../../model/report";
import {ReportService} from "../../services/report/report.service";

export interface DialogDataReport {
  report: ReportWithNames;
}

@Component({
  selector: 'app-report-dialog',
  templateUrl: './report-dialog.component.html',
  styleUrls: ['./report-dialog.component.css']
})
export class ReportDialogComponent implements OnInit {

  serializedDate = new FormControl((new Date()).toISOString());

  createDialog: boolean = true;
  filter = '';
  shops: Shop[];
  employees: Employee[];

  reportId: ReportId = {
    dateReport: new Date(),
    idEmployee: 0,
    nip: ''
  }

  reportFull: ReportWithNames = {
    id: this.reportId,
    income: 0,
    costs: 0,
    sold: 0,
    shopName: '',
    surnameEmployee: '',
    nameEmployee: ''

  }

  reportToSend: Report = {
    id: this.reportId,
    income: 0,
    costs: 0,
    sold: 0,
  }


  constructor(@Inject(MAT_DIALOG_DATA) public  dialogDatareport: DialogDataReport,
              public matDialogRef: MatDialogRef<ReportDialogComponent>,
              private reportService: ReportService, private shopService: ShopService,
              private employeeService: EmployeeService, private snackBar: MatSnackBar,
              private contractService: ContractService) {
  }

  ngOnInit() {
    let selectReport: ReportWithNames = this.dialogDatareport.report;
    if (selectReport != null) {
      this.createDialog = false;
      this.reportFull.id = selectReport.id;
      this.reportFull.income = selectReport.income;
      this.reportFull.costs = selectReport.costs;
      this.reportFull.sold = selectReport.sold;
      this.reportFull.shopName = selectReport.shopName;
      this.reportFull.nameEmployee = selectReport.nameEmployee;
      this.reportFull.surnameEmployee = selectReport.surnameEmployee;
    }
    else {
      this.employeeService.getEmployees().subscribe(employees => this.employees = employees);
    }
  }

  onNoClick() {
    this.matDialogRef.close();
  }

  create() {
    this.reportToSend.id = new ReportId();
    //contractTmp.id = this.contractDto.id;
    this.reportToSend.id.idEmployee = this.reportFull.id.idEmployee;
    this.reportToSend.id.nip = this.reportFull.id.nip;
    this.reportToSend.id.dateReport = this.reportFull.id.dateReport;
    this.reportToSend.income = this.reportFull.income;
    this.reportToSend.costs = this.reportFull.costs;
    this.reportToSend.sold = this.reportFull.sold;

    if (this.createDialog) {
      this.reportService.create(this.reportToSend).subscribe(succes => {
          console.log("poprawnie stworzono sklep");
          this.matDialogRef.close();
        }, error1 => {
          if (error1.status == 422) {
            this.snackBar.open("Podany pracownik dodał już raport dla wybranego sklepu i dnia")
            console.log("blad tworzenia sklepu");
          }
          else if(error1.status == 423){
            this.snackBar.open("W tym dniu, wybrany pracownik nie pracował w danym sklepie")
          }
          else {
            this.snackBar.open("Nie udało się stworzyć raportu, sprawdź czy poprawnie uzupełniłeś wszystkie pola")
          }
        }
      )
    }
    else {
      this.reportService.update(this.reportToSend).subscribe(succes => {
          console.log("poprawnie stworzono sklep");
          this.matDialogRef.close();
        }, error1 => {
          this.snackBar.open("Błąd modyfikacji kontraktu!")

        }
      );
    }
  }


  findShops(idEmployee: number) {
    console.log("wykonano findShop");
    this.contractService.getShopsWorker(idEmployee).subscribe(findShops => {
        this.shops = findShops;
        console.log("poprawnie stworzono sklep");
      }, error1 => {
        if (error1.status == 422) {
          this.snackBar.open("Contakt o podanym kluczy już istnieje!!")
          console.log("blad tworzenia sklepu");
        }
        else {
          this.snackBar.open("Coś poszło nie tak, sprawdź czy poprawnie uzupełniłeś wszystkie pola")
        }
      }
    )
  }

  validationInput() {
    if (this.reportFull.income >= 0 && this.reportFull.costs >= 0 && this.reportFull.sold >= 0) {
      return true;
    }
    else return false;
  }
}
