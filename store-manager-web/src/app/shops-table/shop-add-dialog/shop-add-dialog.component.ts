import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from "@angular/material";
import {Shop} from "../../model/shop";
import {ShopService} from "../../services/shop/shop.service";
import {catchError} from "rxjs/operators";
import {HttpErrorResponse} from "@angular/common/http";

export interface dialogDataShop {
  shop: Shop;
}

@Component({
  selector: 'app-shop-add-dialog',
  templateUrl: './shop-add-dialog.component.html',
  styleUrls: ['./shop-add-dialog.component.css']
})
export class ShopAddDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public dialogDataShop: dialogDataShop,
              public matDialogRef: MatDialogRef<ShopAddDialogComponent>,
              private shopService: ShopService, public snackBar: MatSnackBar) {
  }

  shop: Shop = {
    nip: '',
    address: '',
    name: '',
    password: ''
  };
  createDialog: boolean = true;


  ngOnInit() {
    let selectShop  =this.dialogDataShop.shop;
    if (selectShop != null) {
      this.createDialog = false
      this.shop.nip = selectShop.nip;
      this.shop.name = selectShop.name;
      this.shop.address = selectShop.address;
      this.shop.password = selectShop.password;
    }
  }

  onNoClick() {
    this.matDialogRef.close();
  }

  validationInput() {
    let isNumber = new RegExp("^[0-9]+$");
    if (isNumber.test(this.shop.nip) && this.shop.nip.length == 10 &&
      this.shop.name != '' && this.shop.name != '' && this.shop.address != ''
      && this.shop.password != '') {
      return true;
    }
    else false;
  }

  create() {
    if(this.createDialog){
    this.shopService.createShop(this.shop).subscribe(succes => {
        console.log("poprawnie stworzono sklep");
        this.matDialogRef.close();
      }, error1 => {

      console.log(error1.status+"blad tworzenia sklepu");
      this.snackBar.open('Istnieje już firma o podanym numerze NIP');
      }
    )
  }
  else{
      this.shopService.updateShop(this.shop).subscribe(succes => {
          console.log("poprawnie stworzono sklep");
          this.matDialogRef.close();
        }, error1 => {
          console.log(error1.status+"blad tworzenia sklepu");
          this.snackBar.open('Błąd modyfkiacji sklepu, sprawdź czy podałeś prawidłowe dane');
        }
      )
  }
  }
}
