import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopAddDialogComponent } from './shop-add-dialog.component';

describe('ShopAddDialogComponent', () => {
  let component: ShopAddDialogComponent;
  let fixture: ComponentFixture<ShopAddDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopAddDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
