import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSnackBar, MatSort} from '@angular/material';
import {ShopsTableDataSource} from './shops-table-datasource';
import {ShopService} from "../services/shop/shop.service";
import {ShopAddDialogComponent} from "./shop-add-dialog/shop-add-dialog.component";

@Component({
  selector: 'app-shops-table',
  templateUrl: './shops-table.component.html',
  styleUrls: ['./shops-table.component.css'],
})
export class ShopsTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: ShopsTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['nip', 'name', 'address', 'password', 'totalProfit', 'actions'];

  constructor(private shopService: ShopService, private  dialog: MatDialog, private snackBar: MatSnackBar) {
  }


  ngOnInit() {
    this.dataSource = new ShopsTableDataSource(this.paginator, this.sort, this.shopService);
  }

  delete(shop) {
    this.shopService.delete(shop).subscribe(success => {
      this.dataSource.reload();
      console.log('Usunieto');
    }, error => {
      this.snackBar.open("Nie możesz usunąć wybranego sklepu, najpierw usuń wszystkie jego raporty oraz umowy")
      console.log('Blad usuwania');
    });
  }

  openDialog(selectShop): void {
    let dialogRef = this.dialog.open(ShopAddDialogComponent, {
      height: '450px',
      width: '400px',
      data: {shop: selectShop}
    });
    dialogRef.afterClosed()
      .subscribe(value => this.dataSource.reload());
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.reload();
  }

  calculateProfit(nip: string) {
    this.shopService.getTotalProfit(nip).subscribe(value => {
        this.snackBar.open("Średni zysk w raporcie dla wybranej firmy wynosi:  " + value)
      }
      , error1 => {
        console.log("błąd wykonania funkcji")
      })
  }

  deleteReports(nip: string) {
    this.shopService.deleteReports(nip).subscribe(value => this.snackBar.open("Usunięto raporty wybranego sklepu"),
      error1 => console.log("błedne wykonanie procedury"))
  }
}
