import {DataSource} from '@angular/cdk/collections';
import {MatPaginator, MatSort} from '@angular/material';
import {map, startWith, switchMap} from 'rxjs/operators';
import {merge, Observable, Subject} from 'rxjs';
import {ShopService} from "../services/shop/shop.service";
import {Shop} from "../model/shop";

export class ShopsTableDataSource extends DataSource<Shop> {

  private trigger = new Subject();
  public totalElements: number = 0;
  public filter: string='';

  constructor(private paginator: MatPaginator,
              private sort: MatSort,
              private shopService: ShopService) {
    super();

  }


  connect(): Observable<Shop[]> {

    return merge(this.trigger, this.sort.sortChange, this.paginator.page).pipe(
      startWith(null),
      switchMap(() => this.shopService.getAllShops(this.filter,this.paginator.pageIndex, this.paginator.pageSize)),
      map(page => {
        this.totalElements = page.totalElements;
        return page.content;
      })
    );
  }


  disconnect() {
  }

  public reload() {
    this.trigger.next();
  }
}

