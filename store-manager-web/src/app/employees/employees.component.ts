import {Component, OnInit} from '@angular/core';
import {EmployeeService} from "../services/employee/employee.service";
import {Employee} from "../model/employee";
@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employee: Employee;

  navLinks = [
    //{ label: 'Main', path: '/main' },
    {label:'Send daily report', path: '/sendReport'},
    { label: 'Your personal data',path: '/personalData' },
    { path: '', label: 'Logout' },
  ];

  constructor(private employeeService: EmployeeService) {
  }

  ngOnInit() {
    //this.getEmployee(1);
  }

  // getEmployee(idEmployee:number){
  //   this.employeeService.getEmployee(idEmployee).subscribe(value => this.employee = value);
  // }

}
