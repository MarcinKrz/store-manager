import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EmployeesTableComponent} from "./employees-table/employees-table.component";
import {ReportComponent} from "./report/report.component";
import {EmployeesComponent} from "./employees/employees.component";
import {ShopsTableComponent} from "./shops-table/shops-table.component";
import {JobsTableComponent} from "./jobs-table/jobs-table.component";
import {ContractsTableComponent} from "./contracts-table/contracts-table.component";
import {ReportsTableComponent} from "./reports-table/reports-table.component";

const routes: Routes = [
  {path:'', redirectTo:'shops',pathMatch:'full'},
  {path:'employees', component:EmployeesTableComponent},
  {path: 'shops', component: ShopsTableComponent},
  {path:'jobs', component: JobsTableComponent},
  {path: 'contracts', component: ContractsTableComponent},
  {path: 'reports', component: ReportsTableComponent}]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
