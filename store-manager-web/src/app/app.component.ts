import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Store-Manager';
  navLinks = [
    { label:'Shops', path: '/shops'},
    { label: 'Employees',path: '/employees' },
    { label: 'Jobs', path: '/jobs' },
    {label: 'Contracts', path: '/contracts'},
    {label: 'Reports', path: '/reports'}
  ];
}
