export class ReportWithNames {
  id: ReportId;
  income: number;
  costs: number;
  sold: number;
  shopName: string;
  surnameEmployee: string;
  nameEmployee: string;

}

export class ReportId {
  dateReport: Date;
  idEmployee: number;
  nip: String;
}
