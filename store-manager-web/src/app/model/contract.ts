import {ContractPk} from "./contractDto";

export class Contract{
  id: ContractPk;
  toDate: Date;
  salary: number;
}
