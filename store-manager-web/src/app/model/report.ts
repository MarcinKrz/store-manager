import {ReportId} from "./reportWithNames";

export class Report {
  id: ReportId
  income: number;
  costs: number;
  sold: number;
}


