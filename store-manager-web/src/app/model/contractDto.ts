export class ContractDto{
  id: ContractPk;
  toDate: Date;
  salary: number;
  shopName: string;
  surnameEmployee: string;
  nameEmployee:string;
}


export class ContractPk{
    nip: string;
    idEmployee: number;
    jobTitle: string;
    fromDate: Date;
}
