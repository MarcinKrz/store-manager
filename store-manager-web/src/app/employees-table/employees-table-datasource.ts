import {DataSource} from '@angular/cdk/collections';
import {MatPaginator, MatSort} from '@angular/material';
import {merge, Observable, Subject} from 'rxjs';
import {EmployeeService} from "../services/employee/employee.service";
import {map, startWith, switchMap} from "rxjs/operators";
import {Employee} from "../model/employee";

export class EmployeesTableDataSource extends DataSource<Employee> {

  private trigger = new Subject();
  public totalElements: number = 0;
  public filter: string='';

  constructor(private paginator: MatPaginator,
              private sort: MatSort,
              private employeeService: EmployeeService) {
    super();
  }

  connect(): Observable<Employee[]> {
    return merge(this.trigger, this.sort.sortChange, this.paginator.page).pipe( // 'trigger' i 'paginator' zwracają różne typy, ale nam to nie przeszkadza

      startWith(null), // pomocnicze wymuszenie na Observalbu zeby sie wykonal od razu jeden cykl

      switchMap(() => this.employeeService.getAll(this.filter,this.paginator.pageIndex, this.paginator.pageSize)),
      // switchMap jak przyjdzie jakis obiekt w observabu, wykonuje przepiecie na innego observabla - w naszym przypadku
      // gdy zdazy sie cos na "trigger lub paginator" wykonujemy request do servera ktory oddaje Observalbe<Page<Employee>>

      map(page => {
        this.totalElements = page.totalElements; // przy okazji zapisujemy sobie informacje na temat ilosci wszystkich elementow (do uzycia pozniej w paginator w htmlu)
        return page.content;
         // zamieniamy Page<Employee> na Employee[]
      })
    )
  }

  disconnect() {
  }

  public reload() {
    this.trigger.next();
  }


}
