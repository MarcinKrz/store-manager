import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from "@angular/material";
import {Employee} from "../../model/employee";
import {EmployeeService} from "../../services/employee/employee.service";

export interface DialogDataEmployee {
  employee: Employee;
}

@Component({
  selector: 'app-employee-dialog',
  templateUrl: './employee-dialog.component.html',
  styleUrls: ['./employee-dialog.component.css']
})
export class EmployeeDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public  dialogDataEmployee: DialogDataEmployee,
              public matDialogRef: MatDialogRef<EmployeeDialogComponent>,
              private employeeService: EmployeeService,private snackBar: MatSnackBar) {
  }


  employee: Employee = {
    id: 0,
    name: '',
    surname: '',
    address: '',
    password: ''
  };
  createDialog: boolean = true;


  ngOnInit() {
    let selectEmployee: Employee = this.dialogDataEmployee.employee;
    if (selectEmployee != null) {
      this.createDialog = false
      this.employee.id = selectEmployee.id;
      this.employee.name = selectEmployee.name;
      this.employee.surname = selectEmployee.surname;
      this.employee.address = selectEmployee.address;
      this.employee.password = selectEmployee.password;
    }
  }

  onNoClick() {
    this.matDialogRef.close();
  }

  validationInput() {
    if (
      this.employee.name != '' && this.employee.surname != '' && this.employee.address != ''
      && this.employee.password != '') {
      return true;
    }
    else false;
  }

  create() {
    if(this.createDialog){
    this.employeeService.create(this.employee).subscribe(succes => {
        console.log("poprawnie stworzono sklep");
        this.matDialogRef.close();
      }, error1 => {
        this.snackBar.open("Nie udało się stworzyć pracownika, sprawdź czy wpisane przez Ciebie dane są prawidłowe")
        console.log("blad tworzenia sklepu");
      }
    )
  }
  else {
      this.employeeService.update(this.employee).subscribe(succes => {
          console.log("poprawnie zmodyfikowano");
          this.matDialogRef.close();
        }, error1 => {
        this.snackBar.open("Nie udana próba modyfikacji pracownika, sprawdź czy wpisane przez Ciebie dane są prawidłowe")
          console.log("blad update sklepu");
        }
      )}
  }

}
