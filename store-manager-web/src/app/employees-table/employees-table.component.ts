import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MatPaginator, MatSnackBar, MatSort} from '@angular/material';
import { EmployeesTableDataSource } from './employees-table-datasource';
import {EmployeeService} from "../services/employee/employee.service";
import {ShopAddDialogComponent} from "../shops-table/shop-add-dialog/shop-add-dialog.component";
import {Employee} from "../model/employee";
import {EmployeeDialogComponent} from "./employee-dialog/employee-dialog.component";

@Component({
  selector: 'app-employees-table',
  templateUrl: './employees-table.component.html',
  styleUrls: ['./employees-table.component.css'],
})
export class EmployeesTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: EmployeesTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ["surname", 'name',"address","password","actions"];

  constructor(private employeeService: EmployeeService, private dialog: MatDialog,private snackBar: MatSnackBar){}

  ngOnInit() {
    this.dataSource = new EmployeesTableDataSource(this.paginator, this.sort, this.employeeService);
  }



  delete(employee: Employee){
    this.employeeService.delete(employee).subscribe(success => {
      this.dataSource.reload();
      console.log('Usunieto');
    }, error => {
      this.snackBar.open("Nie możesz usunąć pracownika, najpierw usuń jego kontrakty oraz raporty");
      console.log('Blad usuwania');
    });
  }

  openDialog(selectedEmpolyee): void{
    let dialogRef = this.dialog.open(EmployeeDialogComponent, {
      height: '460px',
      width: '400px',
      data: {employee: selectedEmpolyee}
    });
    dialogRef.afterClosed()
      .subscribe( value => this.dataSource.reload());
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.reload();
  }




    /*
    /// alternatywa
    this.paginator.page.subscribe(pageEvent => {
      this.reloadData();
    });
    this.sort.sortChange.subscribe(sort => {
      this.reloadData();
    })
    this.reloadData();
  }


  private reloadData() {
    this.employeeService.getSubordinates(1, this.paginator.pageSize, ...).subscribe(page -> {
      this.paginator.totalElements = page....;
      this.emplyees = page.content;
    })
  }
*/


}
