import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from "@angular/material";
import {ContractDto, ContractPk} from "../../model/contractDto";
import {ContractService} from "../../services/contract/contract.service";
import {FormControl, Validators} from "@angular/forms";
import {ShopService} from "../../services/shop/shop.service";
import {JobService} from "../../services/job/job.service";
import {EmployeeService} from "../../services/employee/employee.service";
import {Shop} from "../../model/shop";
import {Employee} from "../../model/employee";
import {Job} from "../../model/job";
import {Contract} from "../../model/contract";

export interface DialogDataContract {
  contract: ContractDto;
}

@Component({
  selector: 'app-contract-dialog',
  templateUrl: './contract-dialog.component.html',
  styleUrls: ['./contract-dialog.component.css']
})
export class ContractDialogComponent implements OnInit {


  serializedDate = new FormControl((new Date()).toISOString(), [Validators.required]);


  contractPk: ContractPk = {
    nip: '',
    jobTitle: '',
    idEmployee: 0,
    fromDate: new Date()
  };

  contractDto: ContractDto = {
    id: this.contractPk,
    shopName: '',
    surnameEmployee: '',
    nameEmployee: '',
    toDate: new Date(),
    salary: 0
  };

  contractTmp: Contract = {
    id: this.contractPk,
    salary: 0,
    toDate: new Date()
  }

  createDialog: boolean = true;
  filter = '';
  shops: Shop[];
  employees: Employee[];
  jobs: Job[];
  selectJobParametrs: Job = {
    jobTitle: "",
    salaryMin: 0,
    salaryMax: 0
  };

  constructor(@Inject(MAT_DIALOG_DATA) public  dialogDataContract: DialogDataContract,
              public matDialogRef: MatDialogRef<ContractDialogComponent>,
              private contractService: ContractService, private shopService: ShopService, private jobService: JobService,
              private employeeService: EmployeeService, private snackBar: MatSnackBar) {
  }


  ngOnInit() {
    let selectContract: ContractDto = this.dialogDataContract.contract;
    if (selectContract != null) {
      this.createDialog = false;
      this.contractDto.id.nip = selectContract.id.nip;
      this.contractDto.id.idEmployee = selectContract.id.idEmployee;
      this.contractDto.id.jobTitle = selectContract.id.jobTitle;
      this.contractDto.id.fromDate = selectContract.id.fromDate;
      this.contractDto.shopName = selectContract.shopName;
      this.contractDto.surnameEmployee = selectContract.surnameEmployee;
      this.contractDto.nameEmployee = selectContract.nameEmployee;
      this.contractDto.toDate = selectContract.toDate;
      this.contractDto.salary = selectContract.salary;
      //this.selectJobParametrs.jobTitle = selectContract.id.jobTitle;
      this.setSalaryLimit();

    }
    else {
      this.shopService.getAllShops("", 0, 1000).subscribe(
        all => this.shops = all.content)

      this.employeeService.getAll("", 0, 1000).subscribe(
        all => this.employees = all.content)

      this.jobService.getAll("", 0, 1000).subscribe(
        jobs => this.jobs = jobs.content)
    }
  }

  onNoClick() {
    this.matDialogRef.close();
  }

  create() {
    contractTmp: Contract;
    console.log(this.contractDto.id);
    this.contractTmp.id = new ContractPk();
    //contractTmp.id = this.contractDto.id;
    this.contractTmp.id.nip = this.contractDto.id.nip;
    this.contractTmp.id.idEmployee = this.contractDto.id.idEmployee;
    this.contractTmp.id.jobTitle = this.contractDto.id.jobTitle;
    this.contractTmp.id.fromDate = this.contractDto.id.fromDate;
    this.contractTmp.salary = this.contractDto.salary;
    this.contractTmp.toDate = this.contractDto.toDate;
    if (this.createDialog) {
      this.contractService.create(this.contractTmp).subscribe(succes => {
          console.log("poprawnie stworzono sklep");
          this.matDialogRef.close();
        }, error1 => {
          if (error1.status == 422) {
            this.snackBar.open("Contakt o podanym kluczy już istnieje!!")
            console.log("blad tworzenia sklepu");
          }
          else {
            this.snackBar.open("Nieudana próba zapisu, sprawdź czy poprawnie uzupełniłeś wszystkie pola")
          }
        }
      )
    }
    else {
      this.contractService.update(this.contractTmp).subscribe(succes => {
          console.log("poprawnie stworzono sklep");
          this.matDialogRef.close();
        }, error1 => {
          this.snackBar.open("Nieudana próba modyfikacji kontraktu!, sprawdź czy poprawnie uzupełniłeś wszystkie pola")

        }
      );
    }
  }


  validationInput() {
    console.log(this.contractDto);
    //console.log(this.selectJobParametrs);
    if (this.contractDto.salary >= 0
      && this.contractDto.salary >= this.selectJobParametrs.salaryMin
      && this.contractDto.salary <= this.selectJobParametrs.salaryMax
      && new Date(this.contractDto.id.fromDate) <= new Date(this.contractDto.toDate)) {
      return true;
    }
    else false;
  }

  setSalaryLimit() {
    this.jobService.getJob(this.contractDto.id.jobTitle).subscribe(s => {
        this.selectJobParametrs = s;
        this.contractDto.salary = this.selectJobParametrs.salaryMin;
      }
    );
  }


}
