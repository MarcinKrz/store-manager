import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {ContractsTableDataSource} from './contracts-table-datasource';
import {ContractService} from "../services/contract/contract.service";
import {ContractDto} from "../model/contractDto";
import {EmployeeDialogComponent} from "../employees-table/employee-dialog/employee-dialog.component";
import {ContractDialogComponent} from "./contract-dialog/contract-dialog.component";


@Component({
  selector: 'app-contracts-table',
  templateUrl: './contracts-table.component.html',
  styleUrls: ['./contracts-table.component.css'],
})
export class ContractsTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: ContractsTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['nameShop','employee','jobTitle', 'fromDate', 'toDate', 'salary','actions'];

  constructor(private contractService: ContractService,private dialog: MatDialog) {
  }

  ngOnInit() {
    this.dataSource = new ContractsTableDataSource(this.paginator, this.sort, this.contractService);
  }

  delete(contract: ContractDto) {
    console.log(contract.id);
    this.contractService.delete(contract.id).subscribe(success => {
      this.dataSource.reload();
      console.log('Usunieto');
    }, error => {
      console.log('Blad usuwania');
    });
  }

  openDialog(selectContract: ContractDto): void {
      let dialogRef = this.dialog.open(ContractDialogComponent, {
        height: '460px',
        width: '400px',
        data: {contract: selectContract}
      });
    dialogRef.afterClosed()
      .subscribe( value => this.dataSource.reload());
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.reload();
  }


}
