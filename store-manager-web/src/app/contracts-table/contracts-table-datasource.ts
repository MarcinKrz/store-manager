import {DataSource} from '@angular/cdk/collections';
import {MatPaginator, MatSort} from '@angular/material';
import {map, startWith, switchMap} from 'rxjs/operators';
import {merge, Observable, Subject} from 'rxjs';
import {ContractService} from "../services/contract/contract.service";
import {ContractDto} from "../model/contractDto";

export class ContractsTableDataSource extends DataSource<ContractDto> {

  private trigger = new Subject();
  public totalElements: number = 0;
  public filter: string = '';

  constructor(private paginator: MatPaginator, private sort: MatSort, private contractService: ContractService) {
    super();
  }


  connect(): Observable<ContractDto[]> {
    return merge(this.trigger, this.paginator.page, this.sort.sortChange).pipe(
      startWith(null),
      switchMap(() => this.contractService.getAll(this.filter,this.paginator.pageIndex, this.paginator.pageSize)),
      map(page => {
        this.totalElements = page.totalElements;
        console.log(page.content);
        return page.content;

      })
    )
  }


  disconnect() {
  }

  public reload() {
    this.trigger.next();
  }
}
